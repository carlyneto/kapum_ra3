from django.db import models
from datetime import date


class Serie(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=400)
    abierta = models.BooleanField(default=True)
    publicacion = models.CharField(max_length=200)
    genero = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre


class Numero(models.Model):
    serie = models.ForeignKey(Serie, on_delete=models.CASCADE)
    numero = models.IntegerField(default=1)
    buscado = models.IntegerField(default=0)
    descripcion = models.CharField(max_length=400)
    portada = models.ImageField(upload_to='img', default='')
    fecha_publicacion = models.DateField(default=date.today())
    precio = models.FloatField(default=0.0)


    def __str__(self):
        return self.serie.nombre+"_"+str(self.numero)


class Autor(models.Model):
    nombre = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    biografia = models.CharField(max_length=600)
    pais = models.CharField(max_length=600)
    foto = models.ImageField(upload_to='img', default='')

    def __str__(self):
        return self.nombre+" "+self.apellidos


class Comentario(models.Model):
    nombre = models.CharField(max_length=200)
    comentario = models.CharField(max_length=400)
    numero = models.ForeignKey(Numero, on_delete=models.CASCADE)
    fecha = models.DateField(default=date.today(), blank=True)

    def __str__(self):
        return self.comentario
