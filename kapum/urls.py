from django.conf.urls import url

from . import views
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signin', views.signin, name='signin'),
    url(r'^autores', views.autores, name='autores'),
    url(r'^autor/(?P<autor_id>[0-9]+)$', views.autor, name='autor'),
    url(r'^check_login', views.check_login, name='check_login'),
    url(r'^numeros_serie/(?P<serie_id>[0-9]+)$', views.numeros_serie, name='numeros_serie'),
    url(r'^buscador$', views.buscador, name='buscador'),
    url(r'^numeros', views.numeros, name='numeros'),
    url(r'^numero/(?P<numero_id>[0-9]+)/$', views.numero, name='numero'),
    url(r'^nuevo_comentario/(?P<numero_id>[0-9]+)/$', views.nuevo_comentario, name='nuevo_comentario'),

    url(r'^admin', views.admin_login, name='admin_login'),
    url(r'^gestion_numeros_serie/(?P<serie_id>[0-9]+)/$', views.gestion_numeros_serie, name='gestion_numeros_serie'),
    url(r'^gestion_numeros', views.gestion_numeros, name='gestion_numeros'),
    url(r'^form_numero_nuevo/(?P<serie_id>[0-9]+)/$', views.form_numero_nuevo, name='form_numero_nuevo'),
    url(r'^form_numero_editar/(?P<numero_id>[0-9]+)/$', views.form_numero_editar, name='form_numero_editar'),
    url(r'^editar_numero/(?P<numero_id>[0-9]+)$', views.editar_numero, name='editar_numero'),
    url(r'^borrar_numero/(?P<numero_id>[0-9]+)/$', views.borrar_numero, name="borrar_numero"),
    url(r'^nuevo_numero/(?P<serie_id>[0-9]+)/$', views.nuevo_numero, name='nuevo_numero'),

    url(r'^gestion_autores', views.gestion_autores, name='gestion_autores'),
    url(r'^form_autor_nuevo', views.form_autor_nuevo, name='form_autor_nuevo'),
    url(r'^form_autor_editar/(?P<autor_id>[0-9]+)/$', views.form_autor_editar, name='form_autor_editar'),
    url(r'^nuevo_autor', views.nuevo_autor, name='nuevo_autor'),
    url(r'^editar_autor/(?P<autor_id>[0-9]+)$', views.editar_autor, name='editar_autor'),
    url(r'^borrar_autor/(?P<autor_id>[0-9]+)/$', views.borrar_autor, name="borrar_autor"),

    url(r'^gestion_series', views.gestion_series, name='gestion_series'),
    url(r'^form_serie_nueva', views.form_serie_nueva, name='form_serie_nueva'),
    url(r'^form_serie_editar/(?P<serie_id>[0-9]+)/$', views.form_serie_editar, name='form_serie_editar'),
    url(r'^nueva_serie', views.nueva_serie, name='nueva_serie'),
    url(r'^borrar_serie/(?P<serie_id>[0-9]+)/$', views.borrar_serie, name="borrar_serie"),
    url(r'^editar_serie/(?P<serie_id>[0-9]+)$', views.editar_serie, name='editar_serie'),



    url(r'^salir$', views.salir, name="salir"),


]

