from django.shortcuts import render
from .models import Serie, Numero, Autor, Comentario
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect,reverse
import json
from django.http import HttpResponse,HttpResponseRedirect
from .forms import AutorForm, SerieForm, NumeroForm, ComentarioForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from KapumRA3.settings import NUMEROS_POR_PAGINA




def index(request):
    series = Serie.objects.all()
    numeros = Numero.objects.filter(numero=1)
    contexto = {'series': series, 'numeros': numeros}
    return render(request, 'kapum/index.html', contexto)


def buscador(request):
    busqueda = request.POST['busqueda']
    print(busqueda)
    numeros = Numero.objects.filter(descripcion__contains=busqueda)

    for numero in numeros:
        numero.buscado = numero.buscado+1
        numero.save()

    contexto = {'numeros': numeros}
    print(numeros)
    return render(request, 'kapum/busqueda.html', contexto)


def numeros_serie(request, serie_id):
    numeros = Numero.objects.filter(serie_id=serie_id)
    serie = Serie.objects.get(pk=serie_id);
    contexto = {'numeros': numeros, 'serie': serie}
    return render(request, 'kapum/numeros_serie.html', contexto)


def numeros(request):
    numeros = Numero.objects.order_by("serie")
    pagina = request.GET.get('pagina', 1)
    paginator = Paginator(numeros, NUMEROS_POR_PAGINA)
    try:
        numeros = paginator.page(pagina)
    except PageNotAnInteger:
        numeros = paginator.page(1)
    except EmptyPage:
        numeros = paginator.page(paginator.num_pages)

    contexto = {'numeros': numeros}
    return render(request, 'kapum/numeros_serie.html', contexto)


def numero(request, numero_id):
    numero = Numero.objects.get(pk=numero_id)
    comentarios = Comentario.objects.filter(numero_id=numero_id)
    comentario = Comentario()
    form = ComentarioForm(instance=comentario)
    contexto = {'numero': numero, 'comentarios': comentarios, 'form': form}
    return render(request, 'kapum/numero.html', contexto)


def nuevo_comentario(request, numero_id):
        if request.method == 'POST':
            form = ComentarioForm(request.POST, request.FILES)
            if form.is_valid():
                comentario = Comentario()
                comentario.numero = numero_id
                comentario.nombre = form.cleaned_data['nombre']
                comentario.comentario = form.cleaned_data['comentario']
                print(comentario)
                comentario.save()
            else:
                return render(request, 'kapum/numero.html', {'form':form})

            return redirect('numero')


def signin(request):
    return render(request, 'kapum/signin.html')


def autores(request):
    autores = Autor.objects.all()
    contexto = {'autores': autores}
    return render(request, 'kapum/autores.html', contexto)


def autor(request, autor_id):
    autor = Autor.objects.get(pk=autor_id)
    contexto = {'autor': autor}
    return render(request, 'kapum/autor.html', contexto)


def check_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('admin/index.html')

    context = {'message': 'Usuario o contraseña erroneos'}
    return render(request, 'signin.html', context)


def admin_login(request):
    if request.user.is_authenticated:
        series = Serie.objects.all()
        contexto = {'series': series}
        return render(request, 'kapum/admin/index.html',contexto)
    else:
        return redirect('signin.html')


def gestion_autores(request):
    if request.user.is_authenticated:
        autores = Autor.objects.all()
        contexto = {'autores': autores}
        return render(request, 'kapum/admin/gestion_autores.html', contexto)
    else:
        return redirect('signin.html')


def gestion_series(request):
    if request.user.is_authenticated:
        series = Serie.objects.all()
        contexto = {'series': series}
        return render(request, 'kapum/admin/gestion_series.html', contexto)
    else:
        return redirect('signin.html')


def gestion_numeros(request):
    if request.user.is_authenticated:
        numeros = Numero.objects.order_by("serie")
        contexto = {'numeros': numeros}
        return render(request, 'kapum/admin/gestion_numeros.html', contexto)
    else:
        return redirect('signin.html')


def gestion_numeros_serie(request, serie_id):
    if request.user.is_authenticated:
        numeros = Numero.objects.filter(serie_id=serie_id)
        serie = Serie.objects.get(pk=serie_id)
        return render(request, 'kapum/admin/gestion_numeros.html', {'numeros': numeros, 'serie': serie})
    else:
        return redirect('signin.html')


def form_numero_nuevo(request, serie_id):
    if request.user.is_authenticated:
        serie = Serie.objects.get(pk=serie_id)
        form = NumeroForm()
        context = {'form': form, 'serie': serie}
        return render(request, 'kapum/admin/form_numero.html',context)
    else:
        return redirect('signin.html')


def form_numero_editar(request, numero_id):
    if request.user.is_authenticated:
        numero = Numero.objects.get(pk=numero_id)
        form = NumeroForm(instance=numero)
        print(numero.precio)
        context = {'form': form, 'numero': numero}
        return render(request, 'kapum/admin/form_numero.html', context)
    else:
        return redirect('signin.html')


def nuevo_numero(request,serie_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = NumeroForm(request.POST, request.FILES)
            if form.is_valid():
                numero = Numero()
                numero.serie_id = serie_id
                numero.numero = form.cleaned_data['numero']
                numero.descripcion = form.cleaned_data['descripcion']
                numero.portada = form.cleaned_data['portada']
                numero.fecha_publicacion = form.cleaned_data['fecha_publicacion']
                numero.precio = form.cleaned_data['precio']
                numero.save()
            else:
                return render(request, 'kapum/admin/form_numero.html', {'form':form})

            return redirect('gestion_numeros')
    else:
        return redirect('signin.html')

def editar_numero(request, numero_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = NumeroForm(request.POST, request.FILES)
            if form.is_valid():
                numero = Numero.objects.get(pk=numero_id)
                numero.numero = form.cleaned_data['numero']
                numero.descripcion = form.cleaned_data['descripcion']
                numero.portada = form.cleaned_data['portada']
                numero.fecha_publicacion = form.cleaned_data['fecha_publicacion']
                numero.precio = form.cleaned_data['precio']
                numero.save()
                return redirect('gestion_numeros')
            else:
                return render(request, 'kapum/admin/form_numero.html', {'form':form})

    else:
        return redirect('signin.html')



def borrar_numero(request, numero_id):
    if request.user.is_authenticated:
        numero = Numero.objects.get(pk=numero_id)
        numero.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin.html')


def borrar_serie(request, serie_id):
    if request.user.is_authenticated:
        serie = Serie.objects.get(pk=serie_id)
        serie.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin.html')



def form_autor_nuevo(request):
    if request.user.is_authenticated:
        form = AutorForm()
        context = {'form': form}
        return render(request, 'kapum/admin/form_autor.html',context)
    else:
        return redirect('signin.html')


def form_autor_editar(request, autor_id):
    if request.user.is_authenticated:
        form = AutorForm()
        autor = Autor.objects.get(pk=autor_id)
        context = {'form': form, 'autor':autor}
        return render(request, 'kapum/admin/form_autor.html',context)
    else:
        return redirect('signin.html')


def nuevo_autor(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = AutorForm(request.POST, request.FILES)
            if form.is_valid():
                autor = Autor()
                autor.nombre = form.cleaned_data['nombre']
                autor.apellidos = form.cleaned_data['apellidos']
                autor.biografia = form.cleaned_data['biografia']
                autor.foto = form.cleaned_data['foto']
                autor.pais = form.cleaned_data['pais']
                autor.save()
            else:
                return render(request, 'kapum/admin/form_autor.html', {'form':form})

            return redirect('gestion_autores')
    else:
        return redirect('signin.html')


def editar_autor(request, autor_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = AutorForm(request.POST, request.FILES)
            if form.is_valid():
                autor = Autor.objects.get(pk=autor_id)
                autor.nombre = form.cleaned_data['nombre']
                autor.apellidos = form.cleaned_data['apellidos']
                autor.biografia = form.cleaned_data['biografia']
                autor.foto = form.cleaned_data['foto']
                autor.pais = form.cleaned_data['pais']
                autor.save()
                return redirect('gestion_autores')
            else:
                return render(request, 'kapum/admin/form_autor.html', {'form':form})

    else:
        return redirect('signin.html')


def borrar_autor(request, autor_id):
    if request.user.is_authenticated:
        autor = Autor.objects.get(pk=autor_id)
        autor.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin.html')


def form_serie_nueva(request):
    if request.user.is_authenticated:
        form = SerieForm()
        context = {'form': form}
        return render(request, 'kapum/admin/form_serie.html',context)
    else:
        return redirect('signin.html')


def form_serie_editar(request, serie_id):
    if request.user.is_authenticated:
        serie = Serie.objects.get(pk=serie_id)
        form = SerieForm()
        context = {'form': form, 'serie': serie}
        return render(request, 'kapum/admin/form_serie.html',context)
    else:
        return redirect('signin.html')


def nueva_serie(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = SerieForm(request.POST, request.FILES)
            if form.is_valid():
                serie = Serie()
                serie.nombre = form.cleaned_data['nombre']
                serie.descripcion = form.cleaned_data['descripcion']
                serie.abierta = form.cleaned_data['abierta']
                serie.publicacion = form.cleaned_data['publicacion']
                serie.genero = form.cleaned_data['genero']
                serie.save()
            else:
                return render(request, 'kapum/admin/form_serie.html', {'form':form})

            return redirect('gestion_series')
    else:
        return redirect('signin.html')


def editar_serie(request, serie_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = SerieForm(request.POST, request.FILES)
            if form.is_valid():
                serie = Serie.objects.get(pk=serie_id)
                serie.nombre = form.cleaned_data['nombre']
                serie.descripcion = form.cleaned_data['descripcion']
                serie.abierta = form.cleaned_data['abierta']
                serie.publicacion = form.cleaned_data['publicacion']
                serie.genero = form.cleaned_data['genero']
                serie.save()
            else:
                return render(request, 'kapum/admin/form_serie.html', {'form':form})

            return redirect('gestion_series')
    else:
        return redirect('signin.html')

def salir(request):
    return HttpResponseRedirect(reverse('index'))




