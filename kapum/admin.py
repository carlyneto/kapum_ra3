from django.contrib import admin
from .models import Serie, Numero, Autor, Comentario

admin.site.register(Numero)
admin.site.register(Serie)
admin.site.register(Autor)
admin.site.register(Comentario)