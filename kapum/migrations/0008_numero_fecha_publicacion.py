# Generated by Django 2.0.2 on 2018-02-15 19:42

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kapum', '0007_auto_20180213_1909'),
    ]

    operations = [
        migrations.AddField(
            model_name='numero',
            name='fecha_publicacion',
            field=models.DateField(default=datetime.date(2018, 2, 15)),
        ),
    ]
