# Generated by Django 2.0.2 on 2018-02-16 13:21

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kapum', '0008_numero_fecha_publicacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='numero',
            name='fecha_publicacion',
            field=models.DateField(default=datetime.date(2018, 2, 16)),
        ),
    ]
