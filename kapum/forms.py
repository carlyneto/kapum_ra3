from django import forms
from .models import Autor,Numero,Serie,Comentario


class AutorForm(forms.ModelForm):
    class Meta:
        fields = ('nombre', 'apellidos', 'biografia', 'foto', 'pais')
        model = Autor


class SerieForm(forms.ModelForm):
    class Meta:
        fields = ('nombre', 'descripcion', 'abierta', 'publicacion', 'genero')
        model = Serie


class NumeroForm(forms.ModelForm):
    class Meta:
        fields = ('numero', 'descripcion', 'portada', 'fecha_publicacion', 'precio')
        model = Numero


class ComentarioForm(forms.ModelForm):
    class Meta:
        fields = ('nombre', 'comentario', 'fecha')
        model = Comentario

